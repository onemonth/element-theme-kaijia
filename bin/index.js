#!/usr/bin/env node

import {program} from 'commander'
import chalk from 'chalk'
import build, {isThemeExists} from '../lib/task.js'
import {resolve} from 'path'
import fs from 'fs'

;(async () => {
  const pkg = JSON.parse(fs.readFileSync(new URL('../package.json', import.meta.url)).toString())
  const __dirname = resolve()
  
  program.version(pkg.version)

  program
    .command('build [themePath]')
    .description('主题色变量存放的路径（默认 ./styles/etk-theme/variable.scss）')
    .option('-f [path]', '打包后的主题存放的路径（默认 ./styles/etk-theme）')
    .action((themePath, options) => {
      
      isThemeExists()
      
      // 主题色变量存放的路径
      const entryPath = resolve(__dirname, themePath || 'styles/etk-theme/variable.scss')
      // 打包后的css存放路径
      const outputPath = resolve(__dirname, options.f || 'styles/etk-theme')
      console.log('主题色存放的路径 => ', chalk.green(`${entryPath}`))
      console.log('打包后存放的路径 => ', chalk.green(`${outputPath}`))
      
      build(entryPath, outputPath)
    })

  program.parse(process.argv)

})()

