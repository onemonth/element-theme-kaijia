element-ui自定义主题色脚手架 2.0.0

# gitee链接 https://gitee.com/onemonth/element-theme-kaijia

# 在项目安装
`npm i element-theme-kaijia -D`
或
`yarn add element-theme-kaijia -D`

# 还需要安装element-ui主题
`npm i element-theme-chalk -D`
或
`yarn add element-theme-chalk -D`

# 帮助
```shell
etk -h
Usage: index [options] [command]

Options:
  -V, --version                output the version number
  -h, --help                   display help for command

Commands:
  build [options] [themePath]  主题色变量存放的路径（默认 ./styles/etk-theme/variable.scss）
  help [command]               display help for command
```

```shell
etk build -h
Usage: index build [options] [themePath]

主题色变量存放的路径

Options:
  -f [path]   打包后的主题存放的路径（默认 ./styles/etk-theme）
  -h, --help  display help for command
```

# 用法
```shell
# 项目内
npx etk build [你存放的主题色变量的scss文件路径] -f [打包后的css存放的目录]
# 如：npx etk build ./styles/variable.scss -f ./styles/etk-theme

# 全局
etk build [你存放的主题色变量的scss文件路径] -f [打包后的css存放的目录]
# 如：etk build ./styles/variable.scss -f ./styles/etk-theme
```

```scss
// 主题色变量文件模板，$etk-theme只是map的变量，随便写，key则是打包后的css文件名
$etk-theme: (
  etk-red: red,
  etk-yellow: yellow,
  etk-green: green
);
```
