const CONTENT_MATCH_REG = /(?<!-)content\s*:\s*([^;\}]+)/g; // 找出伪元素里content那块内容
const UNICODE_MATCH_REG = /[^\x00-\xff]/g; // 找出非单字节符
export function turnUniCode(source) {
  // this.cacheable(); // 利用缓存来提高编译效率
  source = source.replace(CONTENT_MATCH_REG, function (m, p1) {
    return m.replace(UNICODE_MATCH_REG, function (m) {
      return "\\" + m.charCodeAt(0).toString(16); //   m.charCodeAt(0)返回字符串第一个字符的 Unicode 编码，后面再转16进制，前面加一斜杠
    });
  });
  return source;
}
