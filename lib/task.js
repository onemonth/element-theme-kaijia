import sass from "sass";
import * as path from "path";
import ora from "ora";
import autoprefixer from "autoprefixer";
import postcss from "postcss";
import fsExtra from "fs-extra";
import * as fs from "fs";
import chalk from "chalk";
import { turnUniCode } from "./turn.js";

const __dirname = path.resolve();
// element-theme的源路径
const sourcePath = path.resolve(
  __dirname,
  "node_modules/element-theme-chalk/src/"
);

// element-theme的主题色sass的变量
const primaryKey = "$--color-primary";

function fonts(targetPath) {
  fsExtra.copySync(
    path.resolve(sourcePath, "fonts"),
    path.resolve(targetPath, "fonts")
  );
  ora("字体打包完成").succeed();
}

function themes(entryPath, outputPath) {
  const themeStr = fs
    .readFileSync(path.resolve(sourcePath, "index.scss"))
    .toString();
  const themeVariables = fs.readFileSync(entryPath).toString();
  const reg = /\(.*\)/s;
  const result = themeVariables.match(reg);
  const theme = {};
  if (result) {
    const r = result[0].replace(/\n|\r|\(|\)| |'|"/g, "").split(",");
    r.forEach((i) => {
      const [key, value] = i.split(":");
      theme[key] = value;
    });
  }
  console.log(theme);
  // 循环打包主题
  Object.keys(theme).forEach((key) => {
    sass.render(
      {
        data: `
            // ${key}
            // 添加css全局变量
            :root {
              --primary-color: ${theme[key]};
            }
            ${primaryKey}: ${theme[key]};
            ${themeStr}
            // 用key包裹有bug
            // .${key} {
            //   ${primaryKey}: ${theme[key]};
            //   ${themeStr}
            // }
        `,
        file: "index.scss",
        includePaths: [sourcePath],
        outputStyle: "compressed",
      },
      (err, result) => {
        if (err) throw err;
        const o = path.resolve(outputPath, key);
        // 打包目录不存在需要递归创建
        if (!fs.existsSync(o)) {
          fs.mkdirSync(o, {
            recursive: true,
          });
        }

        const css = turnUniCode(result.css.toString());
        postcss([autoprefixer])
          .process(css, { from: undefined })
          .then((res) => {
            // console.log(result.css)
            // TODO：做个是否覆盖原文件的问询
            // inquirer.prompt([
            //   {
            //     type: 'input',
            //     name: 'color',
            //     message: '文件已经存在，是否覆盖：',
            //     default: '#409EFF'
            //   }
            // ]).then(answers => {
            //   const options = {
            //     ...answers,
            //     output: target
            //   }
            //   build(options)
            // })
            fs.writeFileSync(`${o}/index.css`, res.css);
            // 复制字体
            fonts(o);
            ora(`${o}主题打包完成`).succeed();
          });
      }
    );
  });
}

export function isThemeExists() {
  if (
    !fs.existsSync(path.resolve(__dirname, "node_modules/element-theme-chalk"))
  ) {
    console.log(chalk.red("请安装element-theme-chalk"));
    console.log("npm下载 => ", chalk.green("npm i element-theme-chalk -D"));
    console.log("yarn下载 => ", chalk.green("yarn add element-theme-chalk -D"));
    process.exit(0);
  }
}

export default async function (entryPath, outputPath) {
  // fonts(outputPath)
  themes(entryPath, outputPath);
}
